### Contributing
If you would like to contribute to the labs, please fork and submit a pull request. Commit your changes to the `development` branch. The team will review and, if accepted, publish any pull requests.

The goal of a scenario should be to guide the user through troubleshooting a common netork issue.

#### Writing a new lab
Create a new folder for the lab within the `challenges/` directory of this repo. The directory should contain a README, topology and initial configurations for the lab. The configurations should be organized per device using a structure like the following:
```
challenges/1
├── README.md
├── configs
│   ├── leaf01
│   │   └── etc
│   │       ├── frr
│   │       │   ├── daemons
│   │       │   └── frr.conf
│   │       └── network
│   │           └── interfaces
│   ├── leaf02
│   │   └── etc
│   │       ├── frr
│   │       │   ├── daemons
│   │       │   └── frr.conf
│   │       └── network
│   │           └── interfaces
│   ├── server01
│   │   ├── etc
│   │   │   ├── lldpd.d
│   │   │   │   └── port_info.conf
│   │   │   └── network
│   │   │       ├── interfaces
│   │   │       └── interfaces.d
│   │   │           ├── eth0.cfg
│   │   │           └── vagrant.cfg
│   │   └── home
│   │       └── cumulus
│   │           └── ips
│   │               ├── server01
│   │               ├── server02
│   │               └── server03
│   ├── server02
│   │   ├── etc
│   │   │   ├── lldpd.d
│   │   │   │   └── port_info.conf
│   │   │   └── network
│   │   │       ├── interfaces
│   │   │       └── interfaces.d
│   │   │           ├── eth0.cfg
│   │   │           └── vagrant.cfg
│   │   └── home
│   │       └── cumulus
│   │           └── ips
│   │               ├── server01
│   │               ├── server02
│   │               └── server03
│   └── server03
│       ├── etc
│       │   ├── lldpd.d
│       │   │   └── port_info.conf
│       │   └── network
│       │       ├── interfaces
│       │       └── interfaces.d
│       │           ├── eth0.cfg
│       │           └── vagrant.cfg
│       └── home
│           └── cumulus
│               └── ips
│                   ├── server01
│                   ├── server02
│                   └── server03
└── topology.png
```

A new inventory file should be created in the `inventories/` directory matching the number for the new challenge. Within this directory the `group_vars` and `hosts` are defined. The `hosts` file should contain the necessary information for Ansible to connect to the devices associated with the lab. For example:
```
[all:vars]
ansible_user=cumulus
ansible_ssh_pass=CumulusLinux!

[cumulus]
leaf01
leaf02

[server]
server01
server02
server03
```

The contents of the `group_vars` directory defines which files are used by the lab. For an example, see the [group_vars](automation/inventories/1/group_vars) for the first challenge.